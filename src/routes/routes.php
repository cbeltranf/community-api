<?php
use Illuminate\Http\Request;

Route::middleware(['api', 'jwt' ])->group(function () {
Route::group([
    'prefix' => 'api/v1'
], function () {
    Route::apiResource('community', 'Inmovsoftware\CommunityApi\Http\Controllers\V1\CommunityController');

    Route::post('community/image/upload', 'Inmovsoftware\CommunityApi\Http\Controllers\V1\CommunityController@community_image');

    Route::post('community/block/users', 'Inmovsoftware\CommunityApi\Http\Controllers\V1\UserBlockController@store');
    Route::get('community/blocked/users', 'Inmovsoftware\CommunityApi\Http\Controllers\V1\UserBlockController@index');
    Route::get('community/blocked/users/me', 'Inmovsoftware\CommunityApi\Http\Controllers\V1\UserBlockController@my_bloqued_users');
    Route::post('community/unblock/users', 'Inmovsoftware\CommunityApi\Http\Controllers\V1\UserBlockController@unblock');
    Route::post('community/report/post', 'Inmovsoftware\CommunityApi\Http\Controllers\V1\ReportsPostsController@store');

        });
});
