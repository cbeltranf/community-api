<?php

namespace Inmovsoftware\CommunityApi\Models\V1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserBlock extends Model
{
    use SoftDeletes;
    protected $table = "it_userblock_posts";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    protected $dates = ['deleted_at', 'date'];
    protected $fillable = ['it_business_id', 'it_user_id', 'date', 'user_block_id'];



    public function User()
    {
        return $this->belongsTo('Inmovsoftware\UserApi\Models\V1\User', 'it_users_id', 'id');

    }

    public function UserBlocked()
    {
        return $this->belongsTo('Inmovsoftware\UserApi\Models\V1\User', 'user_block_id', 'id');

    }


    public static function scopethisUser($query){
        $query->it_users_id = auth()->user()->id;
    }

}
