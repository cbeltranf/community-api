<?php

namespace Inmovsoftware\CommunityApi\Models\V1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Inmovsoftware\CommunityApi\Models\V1\ReportsPosts as Reports;
use Inmovsoftware\CommunityApi\Models\V1\UserBlock as Bloqueds;

class Community extends Model
{
    use SoftDeletes;
    use \Inmovsoftware\GeneralApi\Traits\V1\helper;

    protected $table = "it_posts";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];
    protected $fillable = ['it_business_id', 'photo', 'text', 'date', 'it_users_id', 'status'];
    protected $appends = [ 'time_ago'];



    public function User()
    {
        return $this->belongsTo('Inmovsoftware\UserApi\Models\V1\User', 'it_users_id', 'id');

    }


    public function scopefilterValue($query, $param)
    {
        $query->orwhere($this->table. ".text", 'like', "%$param%");
    }

    public static function scopethisUser($query){
        $query->it_users_id = auth()->user()->id;
    }

    public function getTimeAgoAttribute()
    {
            return   $this->time_ago($this->date);;

    }

    public function scopeRemoveBlocks($query)
    {
        $Auth_user = auth('api')->user();

        $reported_posts = Reports::where("it_users_id", "=", $Auth_user->id)->where("type","=","P")->select("item_id")->get();
        $bloqued_users = Bloqueds::where("it_business_id", "=", $Auth_user->it_business_id)->select("user_block_id")->get();

        $query->whereNotIn('id', $reported_posts);
        $query->whereNotIn('it_users_id', $bloqued_users);
    }

}
