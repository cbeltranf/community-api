<?php

namespace Inmovsoftware\CommunityApi\Models\V1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportsPosts extends Model
{
    use SoftDeletes;
    protected $table = "it_reports_posts";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at', 'date_evaluation', 'date'];
    protected $fillable = ['it_users_id', 'type', 'item_id', 'report', 'final_status', 'evaluator', 'date_evaluation', 'date'];



    public function User()
    {
        return $this->belongsTo('Inmovsoftware\UserApi\Models\V1\User', 'it_users_id', 'id');

    }


    public function scopefilterValue($query, $param)
    {
        $query->orwhere($this->table. ".report", 'like', "%$param%");
    }

    public static function scopethisUser($query){
        $query->it_users_id = auth()->user()->id;
    }

}
