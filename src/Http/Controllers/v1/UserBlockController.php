<?php
namespace Inmovsoftware\CommunityApi\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Inmovsoftware\CommunityApi\Models\V1\UserBlock;
use Inmovsoftware\CommunityApi\Http\Resources\V1\GlobalCollection;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class UserBlockController extends Controller
{
    public function index(Request $request)
    {
        $filter = "it_users.name"; //$request->input("filterColumn");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";

        if (empty($sortField)) {
            $sortField = "it_users.name";
        }

        $Auth_user = auth('api')->user();

        $item = UserBlock::orderBy($sortField, $sortOrder);
        $item->select('it_userblock_posts.*');
        $item->join("it_users", "it_users.id","=", "it_userblock_posts.user_block_id");
        $item->where("it_userblock_posts.it_business_id", "=", $Auth_user->it_business_id);
        $item->where("it_users.status", "=", "A");
        $item->whereNull("it_users.deleted_at");



        $item->thisUser();

        $item->where($filter, 'like', "%$filterValue%");

        $item->with('UserBlocked.Position');
        if (empty($pageSize)) {
            $pageSize = 10;
        }


        return new GlobalCollection($item->paginate($pageSize));
    }

    public function my_bloqued_users(Request $request)
    {
        $filter = "it_users.name"; //$request->input("filterColumn");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";

        if (empty($sortField)) {
            $sortField = "it_users.name";
        }

        $Auth_user = auth('api')->user();

        $item = UserBlock::orderBy($sortField, $sortOrder);
        $item->select('it_userblock_posts.*');
        $item->join("it_users", "it_users.id","=", "it_userblock_posts.user_block_id");
        $item->where("it_userblock_posts.it_business_id", "=", $Auth_user->it_business_id);
        $item->where("it_userblock_posts.it_user_id", "=", $Auth_user->id);
        $item->whereNull("it_users.deleted_at");
        $item->where("it_users.status", "=", "A");

        $item->thisUser();

        $item->where($filter, 'like', "%$filterValue%");

        $item->with('UserBlocked.Position');
        if (empty($pageSize)) {
            $pageSize = 10;
        }


        return new GlobalCollection($item->paginate($pageSize));
    }



    public function store(Request $request)
    {

        $data = $request->validate([
            "user_block_id" => "required|exists:it_users,id"
        ]);

        $Auth_user = auth('api')->user();

        $cant  = UserBlock::where("user_block_id","=", $data["user_block_id"])
        ->where("it_user_id","=", $Auth_user->id)->count();

        if($cant < 1 ){

            $block = new UserBlock;
            $block->it_business_id = $Auth_user->it_business_id;
            $block->it_user_id = $Auth_user->id;
            $block->date =  Carbon::now()->format('Y-m-d H:i:s');
            $block->user_block_id = $data["user_block_id"];
            $block->save();
        }

        return response()->json(
            [
                'errors' => [
                    'status' => 200,
                    'messages' => [trans('community.userblocked')]
                ]
            ],
            200
        );

    }

    public function show($community)
    {


    }

    public function update(Request $request, $community)
    {


    }


    public function destroy($community)
    {

    }

    public function unblock(Request $request){

        $data = $request->validate([
            "user_block_id" => "required|exists:it_users,id"
        ]);

        $Auth_user = auth('api')->user();

        $block = UserBlock::where("user_block_id","=", $data["user_block_id"]);
        $block->where("it_user_id","=", $Auth_user->id);
        $block->delete();

        return response()->json(
            [
                'errors' => [
                    'status' => 200,
                    'messages' => [trans('community.userunblocked')]
                ]
            ],
            200
        );

    }

}
