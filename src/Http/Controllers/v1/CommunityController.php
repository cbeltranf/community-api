<?php
namespace Inmovsoftware\CommunityApi\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Inmovsoftware\CommunityApi\Models\V1\Community;
use Inmovsoftware\NewsApi\Models\V1\Ratings;
use Inmovsoftware\CommunityApi\Http\Resources\V1\GlobalCollection;
use Berkayk\OneSignal\OneSignalClient as One;
use Inmovsoftware\UserApi\Models\V1\Userloginview as User;
use Inmovsoftware\GeneralApi\Traits\V1\helper;


use Illuminate\Http\Request;
use Carbon\Carbon;

class CommunityController extends Controller
{

    use helper;

    public function index(Request $request)
    {
        $filter = "text";
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";
        $sortField = "date";


        $Auth_user = auth('api')->user();

        $item = Community::orderBy($sortField, $sortOrder);

        if (!empty($filterValue)) {
            $item->where($filter, 'like', "%$filterValue%");
        }

        if (empty($pageSize)) {
            $pageSize = 10;
        }
        $item->with('User');
        $item->where('status', 'A');
        $item->RemoveBlocks();

        $item->where('it_business_id', '=', $Auth_user->it_business_id);

        $item2 =   new GlobalCollection($item->paginate($pageSize));
        $values = json_decode(json_encode($item2));

        foreach($values->data as $k => $item){

            $likes__ = Ratings::where('item_id','=', $item->id )
            ->where('item_type','=', 'P')
            ->where('type','=', 'L')
            ->where('it_users_id','=', $Auth_user->id)
            ->count();
            $values->data[$k]->my_like = $likes__;

            $likes_cant = Ratings::where('item_id','=', $item->id )
            ->where('item_type','=', 'P')
            ->where('type','=', 'L')
            ->count();
            $values->data[$k]->likes_cant = $likes_cant;

            $comments_cant = Ratings::where('item_id','=', $item->id )
            ->where('item_type','=', 'P')
            ->where('type','=', 'C')
            ->count();
            $values->data[$k]->comments_cant = $comments_cant;

            $views_cant = Ratings::where('item_id','=', $item->id )
            ->where('item_type','=', 'P')
            ->where('type','=', 'R')
            ->count();
            $values->data[$k]->views_cant = $views_cant;
        }


        return response()->json($values);
    }



    public function community_image(Request $request)
    {
       $data = $request->validate([
           "file" => "required",
        ]);
        $new_name = time() . '.' . $request->file->extension();
        $path = $request->file->storeAs('community_images', $new_name, 'public');
        $return["path"]= $path;
        return response()->json($return);
    }



    public function store(Request $request)
    {
        $data = $request->validate([
            "it_business_id" => "required|integer|exists:it_business,id",
            "text" => "required",
            "photo" => "nullable",
            "it_users_id" => "required|integer|exists:it_users,id",
            "status" => "required|in:A,C,S,I",
            "notify_all" => "nullable"
        ]);

        if(isset($data["notify_all"])){
        $send_push = $data["notify_all"];
        }else{
        $send_push = 0;
        }

        $data = request()->only("it_business_id", "text", "photo", "it_users_id", "status");
        $data = array_filter($data);
        $data["date"] = Carbon::now();

        $Community = Community::firstOrNew($data);
        $Community->save();
        $return["community"]= $Community;

        if($send_push == 1){

            $Auth_user = auth('api')->user();

            $users_to_notify = User::where("it_business_id", "=", $Auth_user->it_business_id)
            ->where("id", "!=", $Auth_user->id)
            ->whereNotNull('uuid')->select('uuid')->get();
            $uuids = array();
            foreach($users_to_notify as $not){
                $uuids[] = $not->uuid;
            }

            try{
                $parameters = array(
                    "include_player_ids" => $uuids,
                    //"template_id" => "d43f202b-838e-4aef-90c8-307b03e9c121",
                    //"tags" => array("name"=>"Carlos Andres"),
                    "app_id" => "ad16e782-752f-415b-9bfe-61047e39c560",
                    "contents" => array("en"=>"See posts.", "es"=>"Ver publicaciones."),
                    "headings" => array("en" => $Auth_user->name." ". $Auth_user->last_name." has created a new community post.", "es"=> $Auth_user->name." ". $Auth_user->last_name."  ha creado una nueva publicación en comunidad."),
                    "subtitle" => array("en" =>  $Auth_user->name." ". $Auth_user->last_name." has created a new community post.", "es"=> $Auth_user->name." ". $Auth_user->last_name."  ha creado una nueva publicación en comunidad."),
                    "android_led_color" => "FF9900FF",
                    "test_type" => "1",
                    "data" => array("section"=>"community", "item_id"=> $Community->id)
                    );

                $message =  new One(env("ONE_SIGNAL_APP_ID"), env("ONE_SIGNAL_REST_API_KEY"), env("ONE_SIGNAL_AUTH_KEY"));
                $message->sendNotificationCustom($parameters);
            }catch(Exception $ex){


                return response()->json($Community);

                         }
            }

        return response()->json($Community);

    }


    /**
     * Display the specified resource.
     *
     * @param  Inmovsoftware\CommunityApi\Models\V1\Community $community
     * @return \Illuminate\Http\Response
     */
    public function show(Community $community)
    {

    $Auth_user = auth('api')->user();

    $item = new Ratings;
    $item->item_id =  $community->id;
    $item->it_users_id = $Auth_user->id;
    $item->type = 'R';
    $item->item_type = 'P';
    $item->date = Carbon::now();
    $item->save();

    $community->User;



    $likes__ = Ratings::where('item_id','=', $community->id )
    ->where('item_type','=', 'P')
    ->where('type','=', 'L')
    ->where('it_users_id','=', $Auth_user->id)
    ->count();
    $community->my_like = $likes__;

    $likes_cant = Ratings::where('item_id','=', $community->id )
    ->where('item_type','=', 'P')
    ->where('type','=', 'L')
    ->count();
    $community->likes_cant = $likes_cant;

    $comments_cant = Ratings::where('item_id','=', $community->id )
    ->where('item_type','=', 'P')
    ->where('type','=', 'C')
    ->count();
    $community->comments_cant = $comments_cant;

    $views_cant = Ratings::where('item_id','=', $community->id )
    ->where('item_type','=', 'P')
    ->where('type','=', 'R')
    ->count();
    $community->views_cant = $views_cant;



        return response()->json($community);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Inmovsoftware\CommunityApi\Models\V1\Community $community
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Community $community)
    {
        $data = $request->validate([
            "it_business_id" => "required|integer|exists:it_business,id",
            "name" => "required|max:50",
            "status" => "nullable|in:A,C"
        ]);

        $community->update($data);
        return response()->json($community);



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Inmovsoftware\CommunityApi\Models\V1\Community $community
     * @return \Illuminate\Http\Response
     */
    public function destroy(Community $community)
    {
        $item = $community->delete();
        if ($item) {
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => [trans('exceptions.element_not_found')]
                    ]
                ],
                401
            );
                    } else {
            return response()->json($item);
                    }
    }



}
