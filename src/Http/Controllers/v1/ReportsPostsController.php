<?php
namespace Inmovsoftware\CommunityApi\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Inmovsoftware\CommunityApi\Models\V1\ReportsPosts;
use Inmovsoftware\CommunityApi\Http\Resources\V1\GlobalCollection;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class ReportsPostsController extends Controller
{
    public function index(Request $request)
    {

    }

    public function store(Request $request)
    {
        $data = $request->validate([
            "type" => "required|in:F,P",
            "item_id" => "required|integer",
            "report" => "required|in:S,I"
        ]);

        $Auth_user = auth('api')->user();

        $item = new ReportsPosts;
        $item->it_users_id = $Auth_user->id;
        $item->type = $data["type"];
        $item->date =  Carbon::now()->format('Y-m-d H:i:s');
        $item->item_id = $data["item_id"];
        $item->report = $data["report"];
        $item->save();

        if($data["type"] == 'P'){
         $type = trans('community.thepost');
            }else{
         $type = trans('community.thecomment');
        }

        return response()->json(
            [
                'errors' => [
                    'status' => 200,
                    'messages' => [trans('community.reportcommunity', ['type' => $type])]
                ]
            ],
            200
        );

    }

    public function show( $community)
    {

    }


    public function update(Request $request, $community)
    {



    }

    public function destroy($community)
    {

    }



}
